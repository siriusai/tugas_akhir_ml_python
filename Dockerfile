FROM  python:3.7.9-buster
ENV PYTHONUNBUFFERED=1
RUN apt-get update && apt-get install -y cmake wget
RUN apt install -y python3-opencv
RUN wget https://github.com/Kitware/CMake/releases/download/v3.15.2/cmake-3.15.2.tar.gz
RUN tar -zxvf cmake-3.15.2.tar.gz && cd cmake-3.15.2 && ./bootstrap && make && make install
WORKDIR /code
COPY rs.txt /code/
RUN  pip install -r rs.txt
COPY . /code/
CMD ["python3","manage.py","runserver","0.0.0.0:8080"]
