from django.core.files.storage import FileSystemStorage

IMAGE_FILES_TYPE = ['png', 'jpg']
# Create your views here.
from django.shortcuts import render, redirect
from polls.forms import DocumentForm
from mtcnn.mtcnn import MTCNN
import cv2
import glob
import argparse
import os, sys
from os import listdir, makedirs
import shutil
import numpy as np

img_path = "static/img"
bbbox_path = "static/bbox"
blur_path = "static/blur"
comparison_path = "static/comparison"
data_set = ''


def index(request):
    myfile = request.FILES.getlist('myfile', '')
    video = request.FILES.get('video_datas', '')
    return render(request, 'pages/layout/index.html', {
        'video_data': video,
        'multifoto': myfile
    })


def download(request):
    myfile = request.FILES.getlist('myfile', '')
    video = request.FILES.get('video_datas', '')
    if request.method == 'POST':
        print(request.method, 'tgtgt', myfile, video)
        fs = FileSystemStorage()
        if myfile != '' and video != '':
            for f in myfile:
                fs.save('static/img/' + f.name, f)
            fs.save('static/video/' + video.name, video)
            video_to_images('static/video/' + video.name)
            video_blur('static/video/' + video.name, myfile)
            return render(request, 'pages/layout/download.html', {
                'video_data': video,
                'multifoto': myfile,
                "video_bllur": video.name[:len(video.name) - 4] + '_comparison.mp4'
            })
        else:
            return render(request, 'pages/layout/download.html', {
                'video_data': '',
                'multifoto': ''
            })
    return render(request, 'pages/layout/download.html', {
        'video_data': video,
        'multifoto': myfile
    })


def video_to_images(video):
    print(video)
    vidcap = cv2.VideoCapture(video)
    count = 0
    while vidcap.isOpened():
        success, image = vidcap.read()
        if success:
            cv2.imwrite('static/img_conver_video/%dnm.png' % count, image)
            count += 1
        else:
            break
    cv2.destroyAllWindows()
    vidcap.release()


def video_blur(video_name, foto):
    source_path = sorted(glob.glob("static/img_conver_video/*.png"))
    source_path = source_path[1:len(source_path)]
    tmp = cv2.imread(source_path[0])
    height, width, layers = tmp.shape

    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    video = cv2.VideoWriter(video_name, fourcc, 25.0, (width, height))
    video_comparison = cv2.VideoWriter(video_name[:len(video_name) - 4] + "_comparison.mp4", fourcc, 25.0,
                                       (width * 2, height))

    for i, fi in enumerate(source_path):

        # if i > 10:
        # 	break

        print(fi)
        img = cv2.imread(fi)
        bbox_img = img.copy()
        blur = img.copy()

        detector = MTCNN()
        if not detector.detect_faces(img) == []:
            for j in range(len(detector.detect_faces(img))):
                pos = detector.detect_faces(img)[j]["box"]
                print(pos)
                bbox_img = cv2.rectangle(bbox_img, (pos[0], pos[1]), (pos[0] + pos[2], pos[1] + pos[3]), (0, 255, 0), 3)
                blur[pos[1]:pos[1] + pos[3], pos[0]:pos[0] + pos[2]] = cv2.blur(
                    blur[pos[1]:pos[1] + pos[3], pos[0]:pos[0] + pos[2]], (40, 40))

        comparison_img = np.hstack((bbox_img, blur))

        cv2.imwrite(bbbox_path + "/%05d.jpg" % i, bbox_img)
        cv2.imwrite(blur_path + "/%05d.jpg" % i, blur)
        cv2.imwrite(comparison_path + "/%05d.jpg" % i, comparison_img)

        video.write(blur)
        video_comparison.write(comparison_img)
    video.release()
    video_comparison.release()
    filelist = glob.glob(os.path.join("static/img_conver_video/", "*.png"))
    for f in filelist:
        os.remove(f)
